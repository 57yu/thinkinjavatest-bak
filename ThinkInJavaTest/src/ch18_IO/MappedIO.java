package ch18_IO;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.IntBuffer;
import java.nio.channels.FileChannel;

public class MappedIO {
	
	static String file = "D:/text.txt";
	
	private static int numOfInts = 4000000;
	private static int numOfUbuffInts = 20000;
	private abstract static class Tester {
		private String name;
		public Tester(String name) {
			this.name = name;
		}
		public void runTest() {
			System.out.println(name + ": ");
			try {
				long start = System.nanoTime();
				test();
				double duration = System.nanoTime() - start;
				System.out.format("%.2f\n", duration/1.0e9);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		public abstract void test() throws IOException;
	}
	private static Tester[] tests = {
		new Tester("Stream Write") {

			@Override
			public void test() throws IOException {
				DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(new File(file))));
				for (int i = 0; i < numOfInts ; i++) 
					dos.writeInt(i);
				dos.close();
			}
			
		},
		new Tester("Mapped Write") {

			@Override
			public void test() throws IOException {
				FileChannel fc = new RandomAccessFile(new File(file), "rw").getChannel();
				IntBuffer ib = fc.map(FileChannel.MapMode.READ_WRITE, 0, fc.size()).asIntBuffer();
				for (int i = 0; i < numOfInts; i++)
					ib.put(i);
				fc.close();
			}
			
		},
		new Tester("Stream Reader") {

			@Override
			public void test() throws IOException {
				DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(new File(file))));
				for (int i = 0 ; i < numOfInts; i++)
					dis.readInt();
				dis.close();
			}
			
		},
		new Tester("Mapped Reader") {

			@Override
			public void test() throws IOException {
				FileChannel fc = new RandomAccessFile(new File(file), "rw").getChannel();
				IntBuffer ib = fc.map(FileChannel.MapMode.READ_WRITE, 0, fc.size()).asIntBuffer();
				for (int i = 0; i < numOfInts; i++)
					ib.get();
				fc.close();
			}
			
		},
		new Tester("Sream Read/Writer") {

			@Override
			public void test() throws IOException {
				RandomAccessFile raf = new RandomAccessFile(new File(file), "rw");
				raf.writeInt(1);
				for (int i = 0; i < numOfUbuffInts; i++) {
					raf.seek(raf.length() - 4);
					raf.writeInt(raf.readInt());
				}
				raf.close();
			}
			
		},
		new Tester("Mapped Read/Wtite") {

			@Override
			public void test() throws IOException {
				FileChannel fc = new RandomAccessFile(new File(file), "rw").getChannel();
				IntBuffer ib = fc.map(FileChannel.MapMode.READ_WRITE, 0, fc.size()).asIntBuffer();
				ib.put(0);
				for (int i = 1 ; i < numOfUbuffInts; i++)
					ib.put(ib.get(i - 1));
				fc.close();
			}
			
		}
	};
	public static void main(String[] args) {
		for (Tester test : tests)
			test.runTest();
	}
}
