package ch18_IO;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * @author yuchao
 * use Gzip eays compress
 */
public class GZIPcomress {
	public static void main(String[] args) throws IOException {
		/*if (args.length == 0) {
			System.out.println("Usage: \nGZIPcompress file\n" + "\tUses GZIP compression to compress "
					+ "the file to test.gz");
			System.exit(1);
		}*/
		String file = "D:/text.gz";
		BufferedReader in = new BufferedReader(new FileReader(file));
		BufferedOutputStream out = new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(file)));
		System.out.println("Writing file");
		int c;
		while ((c = in.read()) != -1)
			out.write(c);
		in.close();
		out.close();
		System.out.println("Reading file");
		BufferedReader in2 = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file))));
		String s;
		while ((s = in2.readLine()) != null)
			System.out.println(s);
			
	}
}
